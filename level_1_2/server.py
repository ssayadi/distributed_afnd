import json
import socket
import sys
from threading import Thread
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from numpy import *
import time
import pandas
import socket, pickle

class ThreadWorker(Thread):
    def __init__(self, sock2):
        Thread.__init__(self)
        self.connection = sock2

    def send(self, tag, value):
        message = str((tag, value))
        self.connection.send(message.encode('utf-8'))

    def recv(self):
        BUFFER_SIZE = 1024
        res = []
        c = BUFFER_SIZE
        while c == BUFFER_SIZE:
            d = self.connection.recv(BUFFER_SIZE).decode()
            res.append(d)
            c = len(d)
        return ''.join(res)


    def recv_int(self):
        return int(self.recv())

    def recv_float(self):
        return float(self.recv())

    def recv_eval(self):
        x = self.recv()
        print('recv_eval', x)
        return eval(x)

class Site:

    def __init__(self, data):
        self.original_data = data
        self.data = data

    def length(self):
        return len(self.data)


def write_results(X):

    X = '\n'.join([str(n) for n in X])
    with open('Result_famd_all_data', 'w') as f:
        f.write(X + '\n' + X)

def main(serverip, nb_client):

    try:
        # create TCP/IP socket
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        # retrieve local hostname
        local_hostname = socket.gethostname()

        # output hostname, domain name and IP address
        print("working on %s with %s" % (local_hostname, serverip))

        # bind the socket to the port 23456
        server_address = (serverip, 23456)
        print('starting up on %s port %s' % server_address)
        sock.bind(server_address)

        # listen for incoming connections (server mode) with one connection at a time
        sock.listen(1)

        clients = []
        DS = [1000,1000,1000,2504,2504,2504,0,0,0]
        Allele_Frequency= [ ]

        for i in range(nb_client):
            # wait for a connection
            print('waiting for a connection')
            connection, client_address = sock.accept()
            # show who connected to us
            print('connection from', client_address)

            client = ThreadWorker(connection)
            client.setDaemon(True)
            # worker.start()

            clients.append(client)

        # for i in range(1):
        #     for client in clients:
        #         client.send(b'Data size', 0)
        #
        #     for client in clients:
        #         size = client.recv() # AF de type <class 'str'>
        #         n = eval(size) #Convertir en list
        #         DS.extend(n)
        # print('Data size', DS)
        import time
        start_time = time.time()
        for i in range(1):
            for client in clients:
                client.send(b'Allele Frequency', "DRB1*03:01")

            import time
            start_time = time.time()

            for client in clients:
                AFc = client.recv() # AF de type <class 'str'>
                #print(AFc)
                AFc = eval(AFc) #Convertir en list
                Allele_Frequency.extend(AFc)
                print (Allele_Frequency)

        #ajouter ds for data size en fonction de id
        for i in range(4):
            for j in Allele_Frequency:
                for c,k in enumerate(DS):
                    if int(j[4])==c+1:
                        j[3]= DS[c]

        print('Allele, Allele Freq, Phenotype freq, Data size, Client')
        for i in Allele_Frequency:
            print(i)

        nplist = np.array(Allele_Frequency) #convertir vers numpy array

        AF = nplist[:, 1] #extraire les frequences alleliques
        PF = nplist[:, 2] #extraire les pourcentages des fréqences allelique
        DS = nplist[:, 3] #extraire les tailles des données

        AF = list(map(float, AF)) #convertir les valeur en float
        PF = list(map(float, PF))
        DS = list(map(float, DS))

        PF = [i * 0.01 for i in PF]

        #print(AF, "AF")
        #print(PF, "PF")
        #print(DS, "DS")


        AFG=0
        for i in AF:
            for j in DS:
                A= i*2*j
            AFG=AFG+A
        AFG= AFG/(2*sum(DS))
        print(AFG, "Global Allele frequency")

        PFG=0
        for i in AF:
            for j in DS:
                A = i*2*j
            PFG = PFG + A
        PFG = PFG / (sum(DS))
        print(PFG, "Global Phenotype frequency")
        print("--- %s seconds ---" % (time.time() - start_time))


    except Exception as e:

        import traceback
        traceback.print_exc()
        print(e)
    finally:
        for client in clients:
            client.connection.close()
        sock.close()
        print('Fermer les connections')

if __name__ == "__main__":

    if len(sys.argv) == 1:
        print('usage: python server.py nb_clients serverip')
        sys.exit(0)

    nb_client = int(sys.argv[1])
    if len(sys.argv) > 2:
        serverip = sys.argv[2]
    else:
        serverip = 'localhost'

    print('server ip:', serverip)
    print('number of clients:', nb_client)

    main(serverip, nb_client)