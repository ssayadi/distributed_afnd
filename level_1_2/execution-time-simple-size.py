import matplotlib.pyplot as plt

simple_size = [1824,15195, 28566, 41937, 58916, 75895]

DistAFND=[0.014, 0.029, 0.031, 0.045, 0.055, 0.060]
AFND= [0.009, 0.021, 0.035, 0.049, 0.067, 0.085]
#t_dpdis=[9, 2.42, 0.87, 0.32, 0.17]

#nbsite = [3,5,7,10,15,20,30,40,50]
#nbsite = [3,5,7,10,15,20,30,40,50]
#DistAFND=[0.04, 0.06, 0.07, 0.11, 0.19, 0.29, 0.68, 1, 1.5]
#AFND= [0.05, 0.07, 0.12, 0.2, 0.47, 0.78, 1.34, 2.86, 4.15]


plt.plot(simple_size, DistAFND, "r-",marker= 'o', markerfacecolor ='red', markersize=5, label="DistAFND" )
plt.plot(simple_size, AFND,"b:", marker=  '*', markerfacecolor ='blue', markersize=5, label="AFND")
#plt.plot(nbsite, t_dpdis, "y--", marker=  '+',markerfacecolor = 'yellow', markersize=5, label="dpdis time(s)")

plt.legend()

plt.xlabel("sample_size")
plt.ylabel("execution time (s)")

plt.show()