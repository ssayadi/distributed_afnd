import matplotlib.pyplot as plt
import pip
import pyCompare
import seaborn as sns
import numpy as np
import scipy.stats
from pyCompare import blandAltman

plt.style.use('ggplot')
from matplotlib import pyplot
from scipy.stats import pearsonr
import statsmodels.api as sm



Allele = ["A*01:01","B*07:05","C*03:04","DQB1*02","DRB1*03:01"]

DistAFND_AF=[0.00295605134565669, 0.0008111144522830732, 0.002204319405980851, 0.0043197794943657525, 0.0062060031717789035]
AFND_AF= [0.00295605134565669, 0.0008111144522830732, 0.002204319405980851, 0.0043197794943657525, 0.0062060031717789035]
#t_dpdis=[9, 2.42, 0.87, 0.32, 0.17]

DistAFND_FF=[0.00591210269131338, 0.0016222289045661465, 0.004408638811961702, 0.008639558988731505, 0.012412006343557807]
AFND_FF= [0.00591210269131338, 0.0016222289045661465, 0.004408638811961702, 0.008639558988731505, 0.012412006343557807]
#################################################################### corrélation
r = np.corrcoef(DistAFND_AF, AFND_AF)
print (r,"=r")

p=scipy.stats.pearsonr(DistAFND_AF, AFND_AF)   # Pearson's r
s=scipy.stats.spearmanr(DistAFND_AF, AFND_AF)    # Spearman's rho
k=scipy.stats.kendalltau(DistAFND_AF, AFND_AF)   # Kendall's tau

print (p,"=p")
print (s,"=s")
print (k,"=k")

coeff_pearson,_ = pearsonr(DistAFND_AF, AFND_AF)

print("coefficient de Pearson = {}".format(coeff_pearson))
#Heatmaps of Correlation Matrices###########################
# r =np.array(r)
# corr_matrix = np.corrcoef(r).round(decimals=2)
#
# fig, ax = plt.subplots()
# im = ax.imshow(corr_matrix)
# im.set_clim(-1, 1)
# ax.grid(False)
# ax.xaxis.set(ticks=(0, 1), ticklabels=('x', 'y'))
# ax.yaxis.set(ticks=(0, 1), ticklabels=('x', 'y'))
# ax.set_ylim(2.5, -0.5)
# for i in range(2):
#     for j in range(2):
#         ax.text(j, i, corr_matrix[i, j], ha='center', va='center',
#                 color='r')
# cbar = ax.figure.colorbar(im, ax=ax, format='% .2f')
# plt.show()
############################################################### affichage
plt.style.use('seaborn')
plt.scatter(DistAFND_AF, AFND_AF,linewidth=0, marker = 's', edgecolors = 'blue')
plt.xlabel("Distributed AFND")
plt.ylabel("Classic AFND")
plt.title('Scatter plot Classic/Distributed AFND')
#plt.legend(facecolor='white')

plt.show()
###################################################################

#blandAltman(DistAFND_AF, AFND_AF, limitOfAgreement=1.96, confidenceInterval=95, confidenceIntervalMethod='approximate', detrend=None, percentage=False)

# def bland_altman_plot(data1, data2, *args, **kwargs):
#     data1     = np.asarray(data1)
#     data2     = np.asarray(data2)
#     mean      = np.mean([data1, data2], axis=0)
#     diff      = data1 - data2                   # Difference between data1 and data2
#     md        = np.mean(diff)                   # Mean of the difference
#     sd        = np.std(diff, axis=0)            # Standard deviation of the difference
#
#     plt.scatter(mean, diff, *args, **kwargs)
#     plt.axhline(md,           color='gray', linestyle='--')
#     plt.axhline(md + 1.96*sd, color='gray', linestyle='--')
#     plt.axhline(md - 1.96*sd, color='gray', linestyle='--')
#
# bland_altman_plot(DistAFND_AF, AFND_AF)
# plt.title('Classic AFND vs Distributed AFND')
# plt.show()

#pyCompare.blandAltman(DistAFND_AF, AFND_AF, percentage=True, title='Classic AFND vs Distributed AFND')

