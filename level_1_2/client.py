
import socket
import numpy as np
import time
import sys
import pickle
import json
from json import JSONEncoder
import pandas

class NumpyArrayEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return JSONEncoder.default(self, obj)

class Site:

    def __init__(self, data):
        self.original_data = data
        self.data = data

    def length(self):
        return len(self.data)

    def Allele_Frequency(self, value):
        #print(self.data.loc[(self.data.ALLELE.str.startswith(value))])
        return (self.data.loc[(self.data.ALLELE.str.startswith (value))])

def main(serverip, clientid):

    try:
        # create TCP/IP socket
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        # retrieve local hostname
        local_hostname = socket.gethostname()

        server_address = (serverip, 23456)
        sock.connect(server_address)
        print("connecting to %s with %s" % (local_hostname, serverip))

        #data = np.array(read_data(clientid))
        data = (read_data(clientid))
        #print('data', data)
        #print('columns', data.columns)

        sitei = Site(data)

        # -----------------------------------------------------------
        def send(message):
            sock.sendall(str(message).encode())
        print('waiting for messages..')

        while True:
            message = sock.recv(64)
            if not message:
                break
            print('Message:', message)
            tag, value = eval(message)
            result = ' '

            if tag == b'size of data':
                result = sitei.length()

            if tag == b'data':
                result = list(sitei.data)

            if tag == b'Allele Frequency':
                #print(type(sitei.Allele_Frequency(value))) # dataframe type
                res = sitei.Allele_Frequency(value)
                new_res = res.assign(Data_size =1000, client= clientid)
                print(new_res)
                result = new_res.values.tolist()

            send(result)


    except Exception as e:
        #print(e)
        #raise e
        import traceback
        traceback.print_exc()
    finally:
        print("Closing connection")
        sock.close()



def read_data(clientid):
    path = "data/data{}.csv".format(int(clientid)-1)
    return pandas.read_table(path, sep=';', header=0)


if __name__ == "__main__":
    if len(sys.argv) != 3:
        print('usage: ./client.py serverip clientid')
        sys.exit(1)
    serverip, clientid = sys.argv[1:]
    main(serverip, clientid)
