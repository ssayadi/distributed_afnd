import matplotlib.pyplot as plt

nbsite = [3,5,7,10,15,20,30]

DistAFND=[0.04, 0.06, 0.07, 0.11, 0.19, 0.31, 0.68]
AFND= [0.05, 0.07, 0.12, 0.25, 0.47, 0.78, 1.34]
#t_dpdis=[9, 2.42, 0.87, 0.32, 0.17]

#nbsite = [3,5,7,10,15,20,30,40,50]
#nbsite = [3,5,7,10,15,20,30,40,50]
#DistAFND=[0.04, 0.06, 0.07, 0.11, 0.19, 0.29, 0.68, 1, 1.5]
#AFND= [0.05, 0.07, 0.12, 0.2, 0.47, 0.78, 1.34, 2.86, 4.15]


plt.plot(nbsite, DistAFND, "r-",marker= 'o', markerfacecolor ='red', markersize=5, label="DistAFND" )
plt.plot(nbsite, AFND,"b:", marker=  '*', markerfacecolor ='blue', markersize=5, label="AFND")
#plt.plot(nbsite, t_dpdis, "y--", marker=  '+',markerfacecolor = 'yellow', markersize=5, label="dpdis time(s)")

plt.legend()

plt.xlabel("Number of sites")
plt.ylabel("execution time (s)")

plt.show()