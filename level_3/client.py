
import socket
import sys
from json import JSONEncoder

import numpy as np
import pandas
import pandas as pd
from pandas import DataFrame

res = pandas.read_table("allel_freq.csv", sep=';', header=0) #nouvelle methode

class NumpyArrayEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return JSONEncoder.default(self, obj)

class Site:

    def __init__(self, data):
        self.original_data = data
        self.data = data

    def length(self):
        return len(self.data)

##########################################################################################
    def Allele_Frequency(self, d, res):
        liste = []
    #récupérer tout les data/ chaque data tu fais le calcul pour le res
        for j in range(len(res)):
            n = 0
            for i in range(0,len(d)):
                for k in range(9):
                    if (res.iloc[j, 0]== d.iloc[i,k]):
                        n=n+1
                        alfreq = n/6
                        res.loc[res.index[j], 'ALLELE FREQ'] = alfreq
                        res.loc[res.index[j], '% INDIV'] = n
                        k = list((res.iloc[j, 0], alfreq, n))
                        liste.append(k)
        #print(liste)
        return(liste)
        #si on voulait les réusultats en dataframe
        #df = DataFrame(liste, columns=['Allele', 'ALLELE FREQ', 'INDIV'])
        #print(df)
        #return df
    #data frame en sortie
###########################################################################################
def main(serverip, clientid):

    try:
        # create TCP/IP socket
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        # retrieve local hostname
        local_hostname = socket.gethostname()

        server_address = (serverip, 23456)
        sock.connect(server_address)
        print("connecting to %s with %s" % (local_hostname, serverip))

        #data = np.array(read_data(clientid))
        data = (read_data(clientid))
        #print('data', data)
        #print('columns', data.columns)

        sitei = Site(data)

        # -----------------------------------------------------------
        def send(message):
            sock.sendall(str(message).encode())
        print('waiting for messages..')

        while True:
            message = sock.recv(64)
            if not message:
                break
            print('Message:', message)
            tag, value = eval(message)
            result = ' '

            # if tag == b'size of data':
            #     result = sitei.length()

            if tag == b'Allele Frequency':
                d=sitei.data #nouvelle methode
                result = sitei.Allele_Frequency(d, res)
                df = DataFrame(result, columns=['Allele', 'ALLELE FREQ', 'INDIV'])
                print(df)
                #print(result)

                #ancien code
                #result = list(sitei.data)
                #print (result)


            send(result)


    except Exception as e:
        #print(e)
        #raise e
        import traceback
        traceback.print_exc()
    finally:
        print("Closing connection")
        sock.close()



def read_data(clientid):
    path = "data/data{}.csv".format(int(clientid))
    return pandas.read_table(path, sep=';', header=None)


if __name__ == "__main__":
    if len(sys.argv) != 3:
        print('usage: ./client.py serverip clientid')
        sys.exit(1)
    serverip, clientid = sys.argv[1:]
    main(serverip, clientid)
