import json
import socket
import sys
from threading import Thread
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from numpy import *
import time
import pandas
import socket, pickle
import pandas as pd
from pandas import DataFrame

from csv import reader
# open file in read mode
data = pandas.read_table("allel_freq.csv",sep=';',header=0)


class ThreadWorker(Thread):
    def __init__(self, sock2):
        Thread.__init__(self)
        self.connection = sock2

    def send(self, tag, value):
        message = str((tag, value))
        self.connection.send(message.encode('utf-8'))

    def recv(self):
        BUFFER_SIZE = 1024
        res = []
        c = BUFFER_SIZE
        while c == BUFFER_SIZE:
            d = self.connection.recv(BUFFER_SIZE).decode()
            res.append(d)
            c = len(d)
        return ''.join(res)


    def recv_int(self):
        return int(self.recv())

    def recv_float(self):
        return float(self.recv())

    def recv_eval(self):
        x = self.recv()
        print('recv_eval', x)
        return eval(x)

class Site:

    def __init__(self, data):
        self.original_data = data
        self.data = data

    def length(self):
        return len(self.data)


def write_results(X):

    X = '\n'.join([str(n) for n in X])
    with open('Result_famd_all_data', 'w') as f:
        f.write(X + '\n' + X)

def main(serverip, nb_client):

    try:
        # create TCP/IP socket
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        # retrieve local hostname
        local_hostname = socket.gethostname()

        # output hostname, domain name and IP address
        print("working on %s with %s" % (local_hostname, serverip))

        # bind the socket to the port 23456
        server_address = (serverip, 23456)
        print('starting up on %s port %s' % server_address)
        sock.bind(server_address)

        # listen for incoming connections (server mode) with one connection at a time
        sock.listen(1)


        clients = []
        #DS = [1000,1000,1000,2504,2504,2504,0,0,0]
        Allele_Frequency= [ ]

        for i in range(nb_client):
            # wait for a connection
            print('waiting for a connection')
            connection, client_address = sock.accept()
            # show who connected to us
            print('connection from', client_address)

            client = ThreadWorker(connection)
            client.setDaemon(True)
            # worker.start()

            clients.append(client)

        # for i in range(1):
        #     for client in clients:
        #         client.send(b'Data size', 0)
        #
        #     for client in clients:
        #         size = client.recv() # AF de type <class 'str'>
        #         n = eval(size) #Convertir en list
        #         DS.extend(n)
        # print('Data size', DS)
        import time
        start_time = time.time()
        for i in range(1):
            for client in clients:
                client.send(b'Allele Frequency', 0)

            import time
            start_time = time.time()

            for client in clients:
                AFc = client.recv() # AF de type <class 'str'>
                #print(AFc) #affichage des données réçu
                AFc = eval(AFc) #Convertir en list
                Allele_Frequency.extend(AFc)
            #print (Allele_Frequency, 'data received en liste')
            Allele_Frequency = pd.DataFrame(Allele_Frequency, columns=['Allele', 'ALLELE FREQ', 'INDIV'])
            #print(Allele_Frequency) #affichage en data frame plus claire

        # for i in Allele_Frequency.itertuples():
        #     print(i.Allele)

        # Nb represente le nombre des individues
        Nb =9 #3*3

        for j in range(len(data)):
            n = 0
            for i in Allele_Frequency.itertuples():
                if (data.iloc[j, 0]== i.Allele):
                    n=n+1
                    alfreq = n / (2 * Nb)
                    ndv=n/Nb
                    data.loc[data.index[j],'ALLELE FREQ']= alfreq
                    data.loc[data.index[j], '% INDIV'] = ndv
                    data.loc[data.index[j], 'nb indiv'] = n
                    #print(data.iloc[j, 0], n, alfreq, ndv)

        #enlever les NaN
        index_with_nan = data.index[data.iloc[:, 1].isnull()]
        data.drop(index_with_nan, 0, inplace=True)

        #print (data)
        with pd.option_context('display.max_rows', None, 'display.max_columns', None):
            print(data)
        data.to_csv('output.csv')

        print("--- %s seconds ---" % (time.time() - start_time))


    except Exception as e:

        import traceback
        traceback.print_exc()
        print(e)
    finally:
        for client in clients:
            client.connection.close()
        sock.close()
        print('Fermer les connections')

if __name__ == "__main__":

    if len(sys.argv) == 1:
        print('usage: python server.py nb_clients serverip')
        sys.exit(0)

    nb_client = int(sys.argv[1])
    if len(sys.argv) > 2:
        serverip = sys.argv[2]
    else:
        serverip = 'localhost'

    print('server ip:', serverip)
    print('number of clients:', nb_client)

    main(serverip, nb_client)
